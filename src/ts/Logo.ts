declare const anime: any;

export class Logo {
 
  private timeline: any;

  constructor() {
    this.timeline = anime.timeline();
    setTimeout(this.animate.bind(this), 1800);
  }

  animate() {
    
    this.timeline
      .add({
        targets: '.header',
        opacity: 1,
        easing: 'easeOutCubic'
      })

  }

}