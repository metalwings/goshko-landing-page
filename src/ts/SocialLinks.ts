declare const anime: any;

export class SocialLinks {
 
  private timeline: any;

  constructor() {
    this.timeline = anime.timeline();
    setTimeout(this.animate.bind(this), 2200);
  }

  animate() {
    
    this.timeline
      .add({
        targets: '.social-links .social-links__item',
        opacity: 1,
        translateY: [-10, 0],
        easing: 'easeOutCubic',
        delay: (el: any, i: number) => {
          return i * 100;
        }
      })

  }

}