declare const anime: any;

export class Loader {

  private timeline: any;

  constructor() {
    this.timeline = anime.timeline();
    setTimeout(this.animate.bind(this), 1700);
  }

  animate() {
    this.timeline
      .add({
        targets: '.progress-indicator-wrapper',
        scaleX: 0,
        easing: 'easeOutCubic'
      });
  }

}