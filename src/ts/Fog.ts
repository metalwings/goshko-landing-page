declare let VANTA: any;
declare let anime: any;
declare let window: any;

export class Fog {

  private timeline: any;
  private props: any;

  constructor() {
    this.timeline = anime.timeline();
    this.props = {
      el: "#bkg",
      highlightColor: 0x1d1d1d,
      midtoneColor: 0xf0f0f,
      lowlightColor: 0x222222,
      baseColor: 0x2a2a2a,
      backgroundAlpha: 1.00,
      speed: 2
    };
    window['test'] = this.props;
    this.createFog();
    setTimeout(this.animate.bind(this), 1600);
  }

  animate() {
    this.timeline
      .add({
        targets: '#bkg',
        opacity: 1,
        easing: 'easeOutCubic'
      })
  }

  public createFog(): void {
    VANTA.FOG(this.props);
  }

}