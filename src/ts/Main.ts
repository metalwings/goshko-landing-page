import { Logo } from "./Logo";
import { SocialLinks } from "./SocialLinks";
import { Fog } from "./Fog";
import { Loader } from "./Loader";

(function(){

  const logo = new Logo();
  const socialLinks = new SocialLinks();
  const fog = new Fog();
  const loader = new Loader();


})();