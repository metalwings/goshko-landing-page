// var debug = process.env.NODE_ENV !== "production";
const path = require("path");
const ConcatPlugin = require("webpack-concat-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: 'development',
  context: __dirname,
  entry: {
    main: ["./src/ts/Main.ts", './src/scss/goshko.scss']
  },
  output: {
    path: path.resolve(__dirname + "/dist"),
    filename: "[name].bundle.js"
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    }),
    new ConcatPlugin({
      name: 'libs',
      outputPath: './',
      fileName: '[name].js',
      filesToConcat: [
        path.resolve(__dirname, './src/assets/lib/three.min.js'),
        path.resolve(__dirname, './src/assets/lib/vanta.fog.min.js'),
        path.resolve(__dirname, './src/assets/lib/anime.min.js')
      ]
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new CopyWebpackPlugin([
      { from: './src/assets/', to: './assets'}
    ])
  ],
  module: {
    rules: [{
        test: /\.ts?$/,
        use: 'awesome-typescript-loader',
        exclude: '/node_modules/'
      },
      {
        test: /\.scss?$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          "css-loader",
          "sass-loader"
        ]
      }
    ],
  },
  resolve: {
    extensions: [".ts", '.js', '.scss', '.css']
  }
};