# Goshko Landing Page
This is the source code of my landing page https://goshko.de

__Requirements:__
- node >= 8.9.4 <small>(used @ initial commit)</small>
- npm / yarn

__Used frameworks:__
- [VantaJS](https://www.vantajs.com/)
- [ThreeJS](https://threejs.org/)<sup>r98</sup>

__Installation guide:__

```bash
yarn install
```

__Development__:
```bash
npm run start #creates development server
```

__Deployment__:
```bash
npm run build #prepares files for production
```

Copyright (c) 2018 Georgi Yanev